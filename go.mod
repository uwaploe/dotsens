module bitbucket.org/uwaploe/dotsens

go 1.16

require (
	bitbucket.org/mfkenney/go-tcm v1.7.1
	bitbucket.org/uwaploe/dotpb v0.4.0
	bitbucket.org/uwaploe/go-mensor v0.4.2
	github.com/albenik/go-serial/v2 v2.6.1
	github.com/alicebob/miniredis/v2 v2.31.1
	github.com/charmbracelet/bubbletea v0.25.0
	github.com/charmbracelet/lipgloss v0.9.1
	github.com/gomodule/redigo v1.8.9
	github.com/google/go-cmp v0.5.9 // indirect
	golang.org/x/exp v0.0.0-20240119083558-1b970713d09a
	golang.org/x/sync v0.6.0
	gopkg.in/yaml.v3 v3.0.1
	periph.io/x/conn/v3 v3.7.0
	periph.io/x/host/v3 v3.8.2
)
