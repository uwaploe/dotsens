package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"bitbucket.org/uwaploe/dotsens/internal/sensors"
	yaml "gopkg.in/yaml.v3"
	"periph.io/x/conn/v3/gpio"
	"periph.io/x/conn/v3/gpio/gpioreg"
)

type progConfig struct {
	Redis RedisConfig `yaml:"redis"`
	Pr    SensConfig  `yaml:"pr"`
	Att   AttConfig   `yaml:"att"`
	Sonar SensConfig  `yaml:"sonar"`
}

type RedisConfig struct {
	Addr string `yaml:"addr"`
	Chan string `yaml:"chan"`
}

type SensConfig struct {
	Device string        `yaml:"device"`
	Sw     string        `yaml:"sw"`
	WarmUp time.Duration `yaml:"warm_up"`
}

type AttConfig struct {
	SensConfig `yaml:",inline"`
	Ref        string `yaml:"ref"`
}

var defaultCfg = progConfig{
	Redis: RedisConfig{Addr: "localhost:6379", Chan: "data.sens"},
	Pr:    SensConfig{Device: "/dev/ttymxc4"},
	Att:   AttConfig{SensConfig: SensConfig{Device: "/dev/ttymxc6"}, Ref: "xup0"},
	Sonar: SensConfig{Device: "/dev/ttymxc7"},
}

func InitConfig(path string) (progConfig, error) {
	cfg := defaultCfg
	if path != "" {
		contents, err := os.ReadFile(path)
		if err != nil {
			return cfg, err
		}
		err = cfg.Load(contents)
		if err != nil {
			return cfg, err
		}
	}
	return cfg, nil
}

func (c *progConfig) Load(contents []byte) error {
	return yaml.Unmarshal(contents, c)
}

func (c progConfig) Dump(w io.Writer) error {
	enc := yaml.NewEncoder(w)
	enc.Encode(c) //nolint:errcheck
	return enc.Close()
}

func (c progConfig) Options() ([]sensors.Option, error) {
	opts := make([]sensors.Option, 0)
	opts = append(opts, sensors.PressureDevice(c.Pr.Device))
	opts = append(opts, sensors.AttDevice(c.Att.Device))
	opts = append(opts, sensors.SonarDevice(c.Sonar.Device))
	opts = append(opts, sensors.AttOrientation(c.Att.Ref))

	if c.Pr.Sw != "" {
		if pin := gpioreg.ByName(c.Pr.Sw); pin != nil {
			if err := pin.Out(gpio.Low); err != nil {
				return opts, fmt.Errorf("set %s low: %w", c.Pr.Sw, err)
			}
			opts = append(opts,
				sensors.PressurePower(sensors.PowerCfg{Sw: pin, WarmUp: c.Pr.WarmUp}))
		} else {
			return opts, fmt.Errorf("invalid DIO line: %q", c.Pr.Sw)
		}
	}

	if c.Att.Sw != "" {
		if pin := gpioreg.ByName(c.Att.Sw); pin != nil {
			if err := pin.Out(gpio.Low); err != nil {
				return opts, fmt.Errorf("set %s low: %w", c.Att.Sw, err)
			}
			opts = append(opts,
				sensors.AttPower(sensors.PowerCfg{Sw: pin, WarmUp: c.Att.WarmUp}))
		} else {
			return opts, fmt.Errorf("invalid DIO line: %q", c.Att.Sw)
		}
	}

	if c.Sonar.Sw != "" {
		if pin := gpioreg.ByName(c.Sonar.Sw); pin != nil {
			if err := pin.Out(gpio.Low); err != nil {
				return opts, fmt.Errorf("set %s low: %w", c.Sonar.Sw, err)
			}
			opts = append(opts,
				sensors.SonarPower(sensors.PowerCfg{Sw: pin, WarmUp: c.Sonar.WarmUp}))
		} else {
			return opts, fmt.Errorf("invalid DIO line: %q", c.Sonar.Sw)
		}
	}

	return opts, nil
}
