// Rdsens samples the pressure and attitude sensors at the same rate
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/dotpb"
	"bitbucket.org/uwaploe/dotsens/internal/sensors"
	"bitbucket.org/uwaploe/dotsens/internal/ticker"
	"github.com/gomodule/redigo/redis"
	host "periph.io/x/host/v3"
)

var Version = "dev"
var BuildDate = "unknown"

var Usage = `Usage: rdsens [options] interval [configfile]

Acquire samples from the pressure, attitude, and snow-level sensors on a DOT
Buoy and publish them in Protocol Buffer format to a Redis pub-sub channel.
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	noAtt    bool
	noPr     bool
	noSonar  bool
	dumpCfg  bool
	attDebug bool
)

func parseCmdLine() (progConfig, []sensors.Option, time.Duration) {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.BoolVar(&dumpCfg, "dump", dumpCfg,
		"dump default configuration to stdout and exit")
	flag.BoolVar(&noAtt, "no-att", noAtt, "If true, disable attitude sensor")
	flag.BoolVar(&noPr, "no-pr", noPr, "If true, disable pressure sensor")
	flag.BoolVar(&noSonar, "no-sonar", noSonar, "If true, disable snow-level sensor")
	flag.BoolVar(&attDebug, "att-debug", attDebug,
		"Trace attitude sensor I/O for debugging")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if dumpCfg {
		cfg, _ := InitConfig("")
		cfg.Dump(os.Stdout) //nolint:errcheck
		os.Exit(0)
	}

	interval, err := time.ParseDuration(flag.Arg(0))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid interval: %q\n", flag.Arg(0))
		os.Exit(1)
	}

	cfg, err := InitConfig(flag.Arg(1))
	if err != nil {
		fmt.Fprintf(os.Stderr, "configuration error: %v\n", err)
		os.Exit(1)
	}

	opts, err := cfg.Options()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if noPr {
		opts = append(opts, sensors.PressureDevice(""))
	}

	if noAtt {
		opts = append(opts, sensors.AttDevice(""))
	}

	if noSonar {
		opts = append(opts, sensors.SonarDevice(""))
	}

	if attDebug {
		opts = append(opts, sensors.AttTraceFunc(log.Printf))
	}

	return cfg, opts, interval
}

func main() {
	if _, err := host.Init(); err != nil {
		log.Fatalf("host Init failed: %v", err)
	}

	cfg, opts, interval := parseCmdLine()

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	sampler, err := sensors.New(opts...)
	if err != nil {
		log.Fatal(err)
	}

	var conn redis.Conn
	if cfg.Redis.Chan != "" {
		conn, err = redis.Dial("tcp", cfg.Redis.Addr)
		if err != nil {
			log.Fatalf("Cannot connect to Redis: %v", err)
		}
		defer conn.Close()
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("DOT Buoy sensor sampler (%s)", Version)

	clock := ticker.New(interval)
	defer clock.Stop()

	var wroteHeader bool
	for {
		select {
		case t0 := <-clock.C:
			samp, err := sampler.Sample()
			if err != nil {
				log.Println(err)
			}
			rec := dotpb.NewRecord(t0, samp)
			if cfg.Redis.Chan != "" {
				if frame, err := rec.AsFrame(); err != nil {
					log.Printf("Encoding failed: %v", err)
				} else {
					conn.Do("PUBLISH", cfg.Redis.Chan, frame)
				}
			} else {
				if !wroteHeader {
					fmt.Println(strings.Join(rec.FieldNames(), ","))
					wroteHeader = true
				}
				fmt.Printf("%s\n", rec.AsCsv())
			}
		case <-sigs:
			return
		}
	}
}
