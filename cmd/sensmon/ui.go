package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/uwaploe/dotpb"
	"bitbucket.org/uwaploe/dotsens/internal/rdsub"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/gomodule/redigo/redis"
)

type pair struct {
	key, value string
}

const nEntries = 8

type recStore struct {
	pairs [nEntries]pair
}

func newStore() *recStore {
	return &recStore{}
}

const timeFormat = "2006-01-02 15:04:05.999"

func (r *recStore) UnpackRec(rec *dotpb.Record) {
	t := time.Unix(rec.Time/1e9, rec.Time%1e9)
	r.pairs[0] = pair{key: "Time:", value: t.Format(timeFormat)}
	r.pairs[1] = pair{key: "Pressure (psi):",
		value: fmt.Sprintf("%.3f", float32(rec.Pressure)/dotpb.PressureScale)}
	r.pairs[2] = pair{key: "Heading (°):",
		value: fmt.Sprintf("%.1f", float32(rec.Heading)/dotpb.AngleScale)}
	r.pairs[3] = pair{key: "Pitch (°):",
		value: fmt.Sprintf("%.1f", float32(rec.Pitch)/dotpb.AngleScale)}
	r.pairs[4] = pair{key: "Roll (°):",
		value: fmt.Sprintf("%.1f", float32(rec.Roll)/dotpb.AngleScale)}
	r.pairs[5] = pair{key: "Gyro (°/s):",
		value: fmt.Sprintf("%.3f %.3f %.3f",
			float32(rec.Gyro.X)/dotpb.GyroScale,
			float32(rec.Gyro.Y)/dotpb.GyroScale,
			float32(rec.Gyro.Z)/dotpb.GyroScale)}
	r.pairs[6] = pair{key: "Accel (g):",
		value: fmt.Sprintf("%.3f %.3f %.3f",
			float32(rec.Accel.X)/dotpb.AccelScale,
			float32(rec.Accel.Y)/dotpb.AccelScale,
			float32(rec.Accel.Z)/dotpb.AccelScale)}
	r.pairs[7] = pair{key: "Snow Level (mm):",
		value: fmt.Sprintf("%d", rec.Slevel)}
}

func (r *recStore) Pairs() []pair {
	return r.pairs[:]
}

type modelStyle struct {
	labelStyle lipgloss.Style
	textStyle  lipgloss.Style
	emphStyle  lipgloss.Style
}

type Ui struct {
	ctx    context.Context
	cancel context.CancelFunc
	ch     chan redis.Message
	pool   *redis.Pool
	err    error
	rec    *dotpb.Record
	store  *recStore
	modelStyle
}

type rawMsg redis.Message

func newUi(pool *redis.Pool) Ui {
	ui := Ui{
		pool:  pool,
		rec:   &dotpb.Record{},
		store: newStore(),
		ch:    make(chan redis.Message, 1),
	}
	ui.ctx, ui.cancel = context.WithCancel(context.Background())

	ui.labelStyle = lipgloss.NewStyle().Bold(true).Width(30)
	ui.textStyle = lipgloss.NewStyle().PaddingLeft(4)
	ui.emphStyle = ui.labelStyle.Copy().Foreground(lipgloss.Color("#FF0000"))

	return ui
}

func listenForData(ctx context.Context, pool *redis.Pool, ch chan redis.Message) tea.Cmd {
	psc := redis.PubSubConn{Conn: pool.Get()}
	_ = psc.Subscribe([]interface{}{"data.sens"}...)
	return func() tea.Msg {
		defer psc.Close()
		err := rdsub.ReceiveMsgs(ctx, psc, func(m redis.Message) bool {
			select {
			case ch <- m:
			default:
			}
			return false
		}, nil)
		return err
	}
}

func waitForData(ch chan redis.Message) tea.Cmd {
	return func() tea.Msg {
		return rawMsg(<-ch)
	}
}

func (u Ui) Init() tea.Cmd {
	return tea.Batch(listenForData(u.ctx, u.pool, u.ch),
		waitForData(u.ch))
}

func (u Ui) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case rawMsg:
		if u.err = u.rec.UnmarshalVT(msg.Data[1:]); u.err == nil {
			u.store.UnpackRec(u.rec)
		}
		return u, waitForData(u.ch)
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q", "Q":
			if u.cancel != nil {
				u.cancel()
			}
			return u, tea.Quit
		}
	}

	return u, cmd
}

func (u Ui) renderValue(key, val string) string {
	return lipgloss.JoinHorizontal(lipgloss.Top, u.labelStyle.Render(key),
		u.textStyle.Render(val))
}

func (u Ui) View() string {
	var b strings.Builder
	b.WriteString("DOT Sensor Monitor\n\n")
	if u.err != nil {
		b.WriteString(u.err.Error() + "\n\n")
	} else {
		for _, p := range u.store.Pairs() {
			fmt.Fprintln(&b, u.renderValue(p.key, p.value))
		}
	}

	b.WriteString("\n\n  q/ctrl+c: quit\n")
	return b.String()
}

var _ tea.Model = Ui{}
