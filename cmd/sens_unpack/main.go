// Sens_unpack outputs the contents of a DOT Buoy sensor data file as CSV
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"

	"bitbucket.org/uwaploe/dotpb"
)

var Version = "dev"
var BuildDate = "unknown"

var Usage = `Usage: sens_unpack [options] infile

Convert the DOT Buoy sensor data file (infile) to CSV and write to
standard output.

`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) == 0 {
		fmt.Fprintln(os.Stderr, "Missing input file")
		flag.Usage()
		os.Exit(1)
	}

	f, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[0], err)
	}
	defer f.Close()

	var wroteHeader bool
	for {
		rec, err := dotpb.ReadRecord(f)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Read error: %v", err)
		}
		if !wroteHeader {
			fmt.Println(strings.Join(rec.FieldNames(), ","))
			wroteHeader = true
		}
		fmt.Printf("%s\n", rec.AsCsv())
	}
}
