package sonar

import (
	"context"
	"fmt"
	"math"
	"math/rand"
	"net"
	"testing"
	"time"
)

func runSonarSim(ctx context.Context, addr string, ready chan struct{}) {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		close(ready)
		return
	}

	close(ready)
	conn, err := l.Accept()
	if err != nil {
		return
	}
	defer conn.Close()

	ticker := time.NewTicker(time.Millisecond * 100)
	defer ticker.Stop()

	mean := float64(2500)
	stdDev := float64(10)

	banner := "HRXL-MaxSonar-WRST7 \rMB7374-AXX\rCopr. 2011-2020\rMaxBotix Inc.\rRoHS 24b 150 1220 \rTempI\r"
	conn.Write([]byte(banner))

	for ctx.Err() == nil {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			x := rand.NormFloat64()*stdDev + mean
			if x < 500 {
				x = 500
			}
			// Write simulated sample
			fmt.Fprintf(conn, "R%04d\r", int(x))
		}
	}
}

func relErr(x, y float64) float64 {
	return math.Abs(y-x) / y
}

func TestNextSample(t *testing.T) {
	ready := make(chan struct{})
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go runSonarSim(ctx, ":4242", ready)
	<-ready

	client, err := net.Dial("tcp", ":4242")
	if err != nil {
		cancel()
		t.Fatal(err)
	}
	client.SetReadDeadline(time.Now().Add(time.Second * 10))
	defer client.Close()

	dev := New(client)
	avg, err := dev.Sample(10)
	if err != nil {
		t.Fatal(err)
	}

	if relErr(avg, 2500) > 10./2500. {
		t.Errorf("bad value; expected 2500, got %f", avg)
	}
}
