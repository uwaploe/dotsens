// Package sonar provides an interface to a MaxBotix ultrasonic sonar
// connected to a serial port.
package sonar

import (
	"bufio"
	"bytes"
	"io"
	"strconv"
	"unicode"
)

type Device struct {
	r       *bufio.Reader
	respBuf bytes.Buffer
}

const (
	// Record start character
	START = 'R'
	// Record end character
	EOL = '\r'
)

// Reader states
type rState int

const (
	initState    rState = 0
	readingState rState = 1
	doneState    rState = 2
)

// New returns a new Device attached to r. The sensor must be in
// free-running mode.
func New(r io.Reader) *Device {
	return &Device{r: bufio.NewReader(r)}
}

// Return the next sample from the Device along with any errors that occurred.
func (d *Device) nextSample() (int, error) {
	d.respBuf.Reset()
	state := initState
	digits := int(4)
	// Each sample has the following format
	//
	//.   Rxxxx\r
	//
	// Where xxxx is the target distance in millimeters.
	for state != doneState {
		c, _, err := d.r.ReadRune()
		if err != nil {
			return -1, err
		}

		switch state {
		case initState:
			if c == START {
				c, _, err = d.r.ReadRune()
				if err != nil {
					return -1, err
				}
				if unicode.IsDigit(c) {
					state = readingState
					d.respBuf.WriteRune(c)
					digits--
				}
			}
			continue
		case readingState:
			if unicode.IsDigit(c) {
				d.respBuf.WriteRune(c)
				digits--
				if digits == 0 {
					state = doneState
				}
			} else {
				state = initState
			}
		}
	}
	return strconv.Atoi(d.respBuf.String())
}

// Sample acquires n samples and returns the mean value
func (d *Device) Sample(n int) (float64, error) {
	var alpha, beta, mean float64

	// Discard all buffered data to ensure we get the current value.
	d.r.Discard(d.r.Buffered())
	i := int(1)
	for n > 0 {
		x, err := d.nextSample()
		if err != nil {
			return mean, err
		}
		alpha = 1. / float64(i)
		beta = 1 - alpha
		mean = float64(x)*alpha + beta*mean
		n--
		i++
	}
	return mean, nil
}
