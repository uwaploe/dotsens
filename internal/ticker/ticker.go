// Package ticker implements a clock synchronous version of time.Ticker
package ticker

import "time"

// Clock synchronous version of time.Ticker. The first tick will occur
// a the next multiple of the specified interval.
type SyncTicker struct {
	C      chan time.Time
	cancel chan struct{}
}

func New(interval time.Duration) *SyncTicker {
	tm := SyncTicker{
		C:      make(chan time.Time, 1),
		cancel: make(chan struct{})}

	go func() {
		tnext := time.Now().Truncate(interval).Add(interval).Sub(time.Now())
		select {
		case t := <-time.After(tnext):
			tm.C <- t
		case <-tm.cancel:
			return
		}

		ticker := time.NewTicker(interval)
	loop:
		for {
			select {
			case t := <-ticker.C:
				tm.C <- t
			case <-tm.cancel:
				ticker.Stop()
				break loop
			}
		}
	}()

	return &tm
}

func (t *SyncTicker) Stop() {
	close(t.cancel)
}
