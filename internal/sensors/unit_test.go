package sensors

import (
	"testing"

	tcm "bitbucket.org/mfkenney/go-tcm"
)

func TestDataSample(t *testing.T) {
	vals := []tcm.DataValue{
		{Id: tcm.Heading, Data: float32(345)},
		{Id: tcm.Pangle, Data: float32(-3.14)},
		{Id: tcm.Rangle, Data: float32(1.414)},
		{Id: tcm.AccelX, Data: float32(0.01)},
		{Id: tcm.AccelY, Data: float32(-0.01)},
		{Id: tcm.AccelZ, Data: float32(1)},
		{Id: tcm.GyroX, Data: float32(0.1)},
		{Id: tcm.GyroY, Data: float32(-0.1)},
		{Id: tcm.GyroZ, Data: float32(0)},
	}

	ds := NewSample(42, vals, 123.4)

	if ds.Pressure() != 42 {
		t.Errorf("Bad pressure value: %v", ds.Pressure())
	}

	if ds.Heading() != 345 {
		t.Errorf("Bad heading value: %v", ds.Heading())
	}

	if ds.Pitch() != -3.14 {
		t.Errorf("Bad pitch value: %v", ds.Pitch())
	}

	if ds.Roll() != 1.414 {
		t.Errorf("Bad roll value: %v", ds.Roll())
	}

	if ds.Accel() != [3]float32{0.01, -0.01, 1} {
		t.Errorf("Bad accel value: %v", ds.Accel())
	}

	if ds.Gyro() != [3]float32{0.1, -0.1, 0} {
		t.Errorf("Bad gyro value: %v", ds.Gyro())
	}

	if ds.SnowLevel() != 123 {
		t.Errorf("Bad snow-level value: %v", ds.SnowLevel())
	}

}

func TestEmptySample(t *testing.T) {
	vals := []tcm.DataValue{}

	ds := NewSample(42, vals, 0)

	if ds.Pressure() != 42 {
		t.Errorf("Bad pressure value: %v", ds.Pressure())
	}

	if ds.Heading() != 0 {
		t.Errorf("Bad heading value: %v", ds.Heading())
	}

	if ds.Pitch() != 0 {
		t.Errorf("Bad pitch value: %v", ds.Pitch())
	}

	if ds.Roll() != 0 {
		t.Errorf("Bad roll value: %v", ds.Roll())
	}

	if ds.Accel() != [3]float32{0, 0, 0} {
		t.Errorf("Bad accel value: %v", ds.Accel())
	}

	if ds.Gyro() != [3]float32{0, 0, 0} {
		t.Errorf("Bad gyro value: %v", ds.Gyro())
	}

	if ds.SnowLevel() != 0 {
		t.Errorf("Bad snow-level value: %v", ds.SnowLevel())
	}
}
