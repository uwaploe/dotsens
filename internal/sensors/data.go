package sensors

import (
	"math"
	"strings"

	tcm "bitbucket.org/mfkenney/go-tcm"
	"bitbucket.org/uwaploe/dotpb"
)

// DataSample stores a pressure, attitude, and snow-level sample. It
// implements the dotpb.Sample interface.
type DataSample struct {
	pr  float32
	att map[string]float32
	sl  uint32
}

func NewSample(pr float64, vals []tcm.DataValue, sl float64) DataSample {
	rec := make(map[string]float32)
	for _, val := range vals {
		name := strings.ToLower(val.Id.String())
		rec[name], _ = val.Data.(float32)
	}
	return DataSample{pr: float32(pr), att: rec, sl: uint32(math.Round(sl))}
}

func (s DataSample) Pressure() float32 {
	return s.pr
}

func (s DataSample) Heading() float32 {
	return s.att["heading"]
}

func (s DataSample) Pitch() float32 {
	return s.att["pangle"]
}

func (s DataSample) Roll() float32 {
	return s.att["rangle"]
}

func (s DataSample) Accel() [3]float32 {
	return [3]float32{s.att["accelx"], s.att["accely"], s.att["accelz"]}
}

func (s DataSample) Gyro() [3]float32 {
	return [3]float32{s.att["gyrox"], s.att["gyroy"], s.att["gyroz"]}
}

func (s DataSample) SnowLevel() uint32 {
	return s.sl
}

var _ dotpb.Sample = DataSample{}
