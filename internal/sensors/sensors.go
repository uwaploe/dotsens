// Package sensors provides an interface to the pressure, attitude, and
// snow-level sensors on the DOT buoy.
package sensors

import (
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	tcm "bitbucket.org/mfkenney/go-tcm"
	"bitbucket.org/uwaploe/dotsens/internal/sonar"
	mensor "bitbucket.org/uwaploe/go-mensor"
	serial "github.com/albenik/go-serial/v2"
	"golang.org/x/sync/errgroup"
	"periph.io/x/conn/v3/gpio"
)

// Data fields to acquire from the attitude sensor
var traxDataFields = []tcm.ComponentId{
	tcm.Heading,
	tcm.Pangle,
	tcm.Rangle,
	tcm.AccelX,
	tcm.AccelY,
	tcm.AccelZ,
	tcm.GyroX,
	tcm.GyroY,
	tcm.GyroZ,
}

type PowerCfg struct {
	Sw     gpio.PinOut
	WarmUp time.Duration
	port   *serial.Port
}

func (p PowerCfg) On() error {
	if err := p.Sw.Out(gpio.High); err != nil {
		return err
	}
	time.Sleep(p.WarmUp)
	if p.port != nil {
		p.port.ResetInputBuffer()
	}
	return nil
}

func (p PowerCfg) Off() error {
	return p.Sw.Out(gpio.Low)
}

type sensorOptions struct {
	prDevice      string
	prBaud        int
	burstInterval time.Duration
	burstCount    int
	prCmds        string
	attDevice     string
	attBaud       int
	attMount      string
	sonarDevice   string
	sonarBaud     int
	rdTimeout     time.Duration
	sonarSamples  int
	prPower       PowerCfg
	attPower      PowerCfg
	sonarPower    PowerCfg
	attTrace      func(string, ...interface{})
}

type Option struct {
	f func(*sensorOptions)
}

// Set the pressure sensor serial device
func PressureDevice(name string) Option {
	return Option{func(opts *sensorOptions) {
		opts.prDevice = name
	}}
}

// Set the pressure sensor baud rate
func PressureBaud(baud int) Option {
	return Option{func(opts *sensorOptions) {
		opts.prBaud = baud
	}}
}

func PressurePower(cfg PowerCfg) Option {
	return Option{func(opts *sensorOptions) {
		opts.prPower = cfg
	}}
}

// Set the burst-sampling parameters for the pressure sensor
func BurstSample(count int, interval time.Duration) Option {
	return Option{func(opts *sensorOptions) {
		opts.burstCount = count
		opts.burstInterval = interval
	}}
}

// Set the attitude sensor serial device
func AttDevice(name string) Option {
	return Option{func(opts *sensorOptions) {
		opts.attDevice = name
	}}
}

// Set the attitude sensor baud rate
func AttBaud(baud int) Option {
	return Option{func(opts *sensorOptions) {
		opts.attBaud = baud
	}}
}

// Set the attitude sensor orientation
func AttOrientation(val string) Option {
	return Option{func(opts *sensorOptions) {
		opts.attMount = val
	}}
}

func AttPower(cfg PowerCfg) Option {
	return Option{func(opts *sensorOptions) {
		opts.attPower = cfg
	}}
}

func AttTraceFunc(f func(string, ...interface{})) Option {
	return Option{func(opts *sensorOptions) {
		opts.attTrace = f
	}}
}

// Set the snow-level sensor (sonar) serial device
func SonarDevice(name string) Option {
	return Option{func(opts *sensorOptions) {
		opts.sonarDevice = name
	}}
}

func SonarPower(cfg PowerCfg) Option {
	return Option{func(opts *sensorOptions) {
		opts.sonarPower = cfg
	}}
}

func SonarSamples(n int) Option {
	return Option{func(opts *sensorOptions) {
		opts.sonarSamples = n
	}}
}

// Set the read timeout for serial ports
func ReadTimeout(t time.Duration) Option {
	return Option{func(opts *sensorOptions) {
		opts.rdTimeout = t
	}}
}

// Sampler provides simultaneous sampling of the DOT sensors.
type Sampler struct {
	pr           *mensor.Device
	att          *tcm.Trax
	sonar        *sonar.Device
	attPort      *serial.Port
	closers      []io.Closer
	sonarSamples int
	prPower      PowerCfg
	attPower     PowerCfg
	sonarPower   PowerCfg
}

func New(options ...Option) (*Sampler, error) {
	opts := sensorOptions{
		prDevice:     "/dev/ttymxc4",
		prBaud:       9600,
		attDevice:    "/dev/ttymxc6",
		attBaud:      38400,
		attMount:     "yup0",
		sonarDevice:  "/dev/ttymxc7",
		sonarBaud:    9600,
		rdTimeout:    time.Second * 3,
		sonarSamples: 10,
	}

	for _, opt := range options {
		opt.f(&opts)
	}

	s := &Sampler{}
	s.closers = make([]io.Closer, 0)
	s.sonarSamples = opts.sonarSamples
	s.prPower = opts.prPower
	s.attPower = opts.attPower
	s.sonarPower = opts.sonarPower

	if opts.prDevice != "" {
		p, err := serial.Open(opts.prDevice,
			serial.WithBaudrate(opts.prBaud),
			serial.WithReadTimeout(int(opts.rdTimeout.Milliseconds())))
		if err != nil {
			return s, err
		}
		if s.prPower.Sw != nil {
			s.prPower.port = p
		}
		s.closers = append(s.closers, p)
		s.pr = mensor.New(p)
		err = s.setupMensor(opts.prCmds)
		if err != nil {
			s.Close()
			return s, fmt.Errorf("setup Mensor: %w", err)
		}
	}

	if opts.attDevice != "" {
		tcm.TraceFunc = opts.attTrace
		p, err := serial.Open(opts.attDevice,
			serial.WithBaudrate(opts.attBaud),
			serial.WithReadTimeout(int(opts.rdTimeout.Milliseconds())))
		if err != nil {
			s.Close()
			return s, err
		}
		p.SetFirstByteReadTimeout(uint32(opts.rdTimeout.Milliseconds()))
		if s.attPower.Sw != nil {
			s.attPower.port = p
		}
		s.closers = append(s.closers, p)
		s.att = tcm.NewTrax(p)
		if s.attPower.Sw != nil {
			s.attPower.On()
			time.Sleep(s.attPower.WarmUp)
			defer s.attPower.Off()
		}
		_, err = s.setupTrax()
		if err != nil {
			s.Close()
			return s, fmt.Errorf("setup TRAX: %w", err)
		}
		s.cfgTrax(opts.attMount)
	}

	if opts.sonarDevice != "" {
		p, err := serial.Open(opts.sonarDevice,
			serial.WithBaudrate(opts.sonarBaud),
			serial.WithReadTimeout(int(opts.rdTimeout.Milliseconds())))
		if err != nil {
			s.Close()
			return s, err
		}
		if s.sonarPower.Sw != nil {
			s.sonarPower.port = p
		}
		s.closers = append(s.closers, p)
		s.sonar = sonar.New(p)
	}

	return s, nil
}

func (s *Sampler) setupTrax() (tcm.ModuleInfo, error) {
	info, err := s.att.GetModInfo()
	if err != nil {
		if info, err = s.att.GetModInfo(); err != nil {
			return info, fmt.Errorf("GetModInfo: %w", err)
		}
	}

	if err := s.att.SetDataComponents(traxDataFields...); err != nil {
		return info, err
	}

	if err := s.att.SetAcqParams(tcm.Polled, true, 0, 0); err != nil {
		return info, err
	}

	s.att.SetMagRef()
	s.att.SetMergeRate(time.Second)

	return info, nil
}

func (s *Sampler) cfgTrax(ref string) error {
	if mref, err := tcm.MountingRefValue(ref); err != nil {
		return fmt.Errorf("Bad mounting ref value: %q", ref)
	} else {
		if err := s.att.SetConfig(tcm.Kmountingref, mref); err != nil {
			return err
		}
	}
	return s.att.Save()
}

func (s *Sampler) setupMensor(cmdFile string) error {
	if cmdFile != "" {
		if s.prPower.Sw != nil {
			s.prPower.On()
			time.Sleep(s.prPower.WarmUp)
			defer s.prPower.Off()
		}
		f, err := os.Open(cmdFile)
		if err != nil {
			return fmt.Errorf("open %q: %v", cmdFile, err)
		}
		defer f.Close()
		err = s.pr.Upload(f)
		if err != nil {
			return fmt.Errorf("upload commands: %v", err)
		}
	}
	return nil
}

// Close the serial ports used by the sensors, Sampler must not be used after
// calling this method.
func (s *Sampler) Close() error {
	if s.closers == nil {
		return errors.New("Ports already closed")
	}

	for _, c := range s.closers {
		c.Close()
	}
	s.closers = nil
	return nil
}

// Sample acquires and returns a DataSample
func (s *Sampler) Sample() (DataSample, error) {
	var (
		pr, sl float64
		vals   []tcm.DataValue
	)

	g := new(errgroup.Group)

	// Sample all sensors concurrently

	if s.pr != nil {
		g.Go(func() error {
			var err error
			if s.prPower.Sw != nil {
				err = s.prPower.On()
				if err != nil {
					return fmt.Errorf("gpio %s HIGH: %w",
						s.prPower.Sw.Name(), err)
				}
				time.Sleep(s.prPower.WarmUp)
				defer s.prPower.Off()
			}
			pr, err = s.pr.Pressure()
			if err != nil {
				return fmt.Errorf("pressure sample: %w", err)
			}
			return nil
		})
	}

	if s.att != nil {
		g.Go(func() error {
			var err error
			if s.attPower.Sw != nil {
				err = s.attPower.On()
				if err != nil {
					return fmt.Errorf("gpio %s HIGH: %w",
						s.prPower.Sw.Name(), err)
				}
				time.Sleep(s.attPower.WarmUp)
				defer s.attPower.Off()
			}
			if _, err := s.setupTrax(); err != nil {
				return err
			}

			vals, err = s.att.GetData()
			if err != nil {
				return fmt.Errorf("TRAX sample: %w", err)
			}
			return nil
		})
	}

	if s.sonar != nil {
		g.Go(func() error {
			var err error
			if s.sonarPower.Sw != nil {
				err = s.sonarPower.On()
				if err != nil {
					return fmt.Errorf("gpio %s HIGH: %w",
						s.prPower.Sw.Name(), err)
				}
				time.Sleep(s.sonarPower.WarmUp)
				defer s.sonarPower.Off()
			}
			sl, err = s.sonar.Sample(s.sonarSamples)
			if err != nil {
				return fmt.Errorf("sonar sample: %w", err)
			}
			return nil
		})
	}

	err := g.Wait()
	return NewSample(pr, vals, sl), err
}
